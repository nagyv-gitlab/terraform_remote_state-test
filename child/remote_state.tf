data "terraform_remote_state" "default" {
  backend = "http"

  config = {
    address  = "https://gitlab.com/api/v4/projects/${var.gitlab_project_id}/terraform/state/default"
    username = var.gitlab_username
    password = var.gitlab_access_token
  }
}
