variable "gitlab_access_token" {
  type        = string
  description = "The Gitlab password used to authenticate against the Gitlab Terraform Remote State"
  sensitive   = true
}

variable "gitlab_project_id" {
  type        = string
  description = "The Pharmony Infra Gitlab Project ID"
  sensitive   = true
}

variable "gitlab_username" {
  type        = string
  description = "The Gitlab username used to authenticate against the Gitlab Terraform Remote State"
  sensitive   = true
}
